/**
 * Service class and methods for user APIs
 */

import User from '../../model/user';
import Book from '../../model/book';
import Address from '../../model/address';
interface booktype{
            "isbn": string,
            "title": string,
            "subtitle": string,
            "author": string
}
export class UserService {

    /**
     * Method to get list of dummies
     * @returns {Promise<string[]>}
     */
    public static async get(email: String): Promise<any> {
        const findUser = await User.findOne({"email":email});
        const findAddress = await Address.findOne({"addressId": findUser.address});
        const UserBook = async () => Promise.all(findUser.rentedBooks.map( async (item:booktype) => 
            await Book.findOne({"bookId": item})
        ));
        const BookData = await UserBook();
        const bookList = BookData.map((item:booktype) => ({
            "isbn": item.isbn,
            "title": item.title,
            "subtitle": item.subtitle,
            "author": item.author
        }));
        const response = {
            name: `${findUser.firstname} ${findUser.lastname}`,
            email: findUser.email,
            phone: findUser.phone,
            address: `${findAddress.house},${findAddress.street}, ${findAddress.city} - ${findAddress.postalCode}`,
            country: `${findAddress.country}`,
            rentedBooks: bookList
        };
        return response;
    }
};