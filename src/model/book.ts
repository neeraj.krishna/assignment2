const mongoose = require('mongoose')

var schema = mongoose.Schema;

var bookSchema = new schema({

    bookId: {type:String},
    isbn: {type:String},
    title: {type:String},
    subtitle: {type:String},
    author: {type:String},
    published: {type:String},
    publisher:{type:String},
    pages: {type:Number},
    description: {type:String},
    website:{type:String}

});

var book = mongoose.model('Book', bookSchema);

export default book;
