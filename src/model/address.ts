
const mongoose = require('mongoose');

var schema = mongoose.Schema;

var UserSchema = new schema({

    addressId : {type:String},
    street : {type:String},
    postalCode : {type:Number},
    city : {type:String},
    country : {type:String},
    house : {type:String}
   
});


var address = mongoose.model("Address", UserSchema);

export default address;  