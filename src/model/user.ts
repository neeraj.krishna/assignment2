
const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema({

    firstname: {type:String},
    lastname: {type:String},
    email: {type:String},
    phone: {type:String},
    rentedBooks: {type:Array},
    address: {type:String},
    // rentedBooks: {
    //     type: Schema.Types.ObjectId,
    //     ref: "Book"
    // },
    // address: {
    //     // type: Schema.Types.ObjectId,
    //     // ref: "Address"
    // }
});


var User = mongoose.model("User", UserSchema);

export default User;  